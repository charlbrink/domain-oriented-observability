package org.bitbucket.brinkcharl.doo.infrastructure;

import org.bitbucket.brinkcharl.doo.domain.BirthdayDiscount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ShoppingCartInstrumentationTest {
    private static final String DISCOUNT_CODE_KEY = "discountCode";
    private static final String DISCOUNT_CODE_VALUE = "B0001";

    @Spy
    private Logger logger;

    @Test
    public void givenShoppingCartInstrumentation_whenApplyingDiscountCode_thenExpectInstrumentation() {
        Map<String, Object> extraParams = new HashMap<>();
        extraParams.put(DISCOUNT_CODE_KEY, DISCOUNT_CODE_VALUE);
        ShoppingCartInstrumentation shoppingCartInstrumentation = createObservationContext().createShoppingCartInstrumentation(extraParams);

        shoppingCartInstrumentation.applyingDiscountCode();

        verify(logger, times(1)).trace(anyString(), eq(DISCOUNT_CODE_VALUE));
    }

    @Test
    public void givenShoppingCartInstrumentation_whenDiscountCodeLookupSucceeded_thenExpectInstrumentation() {
        Map<String, Object> extraParams = new HashMap<>();
        extraParams.put(DISCOUNT_CODE_KEY, DISCOUNT_CODE_VALUE);
        ShoppingCartInstrumentation shoppingCartInstrumentation = createObservationContext().createShoppingCartInstrumentation(extraParams);

        shoppingCartInstrumentation.discountCodeLookupSucceeded();

        verify(logger, times(1)).trace(anyString(), eq(DISCOUNT_CODE_VALUE));
    }

    @Test
    public void givenShoppingCartInstrumentation_whenDiscountCodeLookupFailedException_thenExpectInstrumentation() {
        Map<String, Object> extraParams = new HashMap<>();
        extraParams.put(DISCOUNT_CODE_KEY, DISCOUNT_CODE_VALUE);
        ShoppingCartInstrumentation shoppingCartInstrumentation = createObservationContext().createShoppingCartInstrumentation(extraParams);

        shoppingCartInstrumentation.discountCodeLookupFailed(new RuntimeException());

        verify(logger, times(1)).error(anyString(), eq(DISCOUNT_CODE_VALUE), any(Exception.class));
    }

    @Test
    public void givenShoppingCartInstrumentation_whenDiscountCodeLookupFailed_thenExpectInstrumentation() {
        Map<String, Object> extraParams = new HashMap<>();
        extraParams.put(DISCOUNT_CODE_KEY, DISCOUNT_CODE_VALUE);
        ShoppingCartInstrumentation shoppingCartInstrumentation = createObservationContext().createShoppingCartInstrumentation(extraParams);

        shoppingCartInstrumentation.discountCodeLookupFailed();

        verify(logger, times(1)).warn(anyString(), eq(DISCOUNT_CODE_VALUE));
    }

    @Test
    public void givenShoppingCartInstrumentation_whenDiscountApplied_thenExpectInstrumentation() {
        Map<String, Object> extraParams = new HashMap<>();
        extraParams.put(DISCOUNT_CODE_KEY, DISCOUNT_CODE_VALUE);
        ShoppingCartInstrumentation shoppingCartInstrumentation = createObservationContext().createShoppingCartInstrumentation(extraParams);

        shoppingCartInstrumentation.discountApplied(new BirthdayDiscount(new BigDecimal("10.00")), new BigDecimal("10.00"));

        verify(logger, times(1)).info(anyString(), eq(DISCOUNT_CODE_VALUE), eq(new BigDecimal("10.00")));
    }

    private ObservationContext createObservationContext() {
        RequestContext requestContext = RequestContext
                .builder()
                .requestId("requestId")
                .userId("user001")
                .build();
        Map<String, Object> standardParams = new HashMap<>();

        return new ObservationContext(requestContext, standardParams, logger);
    }
}
