package org.bitbucket.brinkcharl.doo.domain;

import org.bitbucket.brinkcharl.doo.infrastructure.ObservationContext;
import org.bitbucket.brinkcharl.doo.infrastructure.RequestContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ShoppingCartTest {
    private static final String VALID_DISCOUNT_CODE = "B0001";
    private static final String INVALID_DISCOUNT_CODE = "Z999";

    @Spy
    private Logger logger;

    @Mock
    private DiscountService discountService;

    private ShoppingCart shoppingCart;

    @Before
    public void setup() {
        shoppingCart = new ShoppingCart(discountService, createObservationContext());
    }

    @Test
    public void givenShoppingCart_whenApplyValidDiscountCode_thenExpectDiscountApplied() {
        Discount birthdayDiscount = new BirthdayDiscount(new BigDecimal(10));
        Optional<Discount> discount = Optional.of(birthdayDiscount);
        when(discountService.lookupDiscount(VALID_DISCOUNT_CODE)).thenReturn(discount);

        shoppingCart.applyDiscountCode(VALID_DISCOUNT_CODE);

        verify(logger, times(2)).trace(anyString(), eq(VALID_DISCOUNT_CODE));
    }

    @Test
    public void givenShoppingCart_whenApplyInValidDiscountCode_thenExpectNoDiscountApplied() {
        Optional<Discount> discount = Optional.empty();
        when(discountService.lookupDiscount(INVALID_DISCOUNT_CODE)).thenReturn(discount);

        shoppingCart.applyDiscountCode(INVALID_DISCOUNT_CODE);

        verify(logger, times(1)).trace(anyString(), eq(INVALID_DISCOUNT_CODE));
    }

    private ObservationContext createObservationContext() {
        RequestContext requestContext = RequestContext
                .builder()
                .requestId("requestId")
                .userId("user001")
                .build();
        Map<String, Object> standardParams = new HashMap<>();

        return new ObservationContext(requestContext, standardParams, logger);
    }


}