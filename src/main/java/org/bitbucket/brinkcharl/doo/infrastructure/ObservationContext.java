package org.bitbucket.brinkcharl.doo.infrastructure;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@AllArgsConstructor
public class ObservationContext {
    private final RequestContext requestContext;
    private final Map<String, Object> standardParams;
    private Logger logger;

    public ShoppingCartInstrumentation createShoppingCartInstrumentation(final Map<String, Object> extraParams){
        Map<String, Object> mergedParams = new HashMap<>();
        mergedParams.putAll(standardParams);
        mergedParams.putAll(extraParams);
        mergedParams.put("requestId", requestContext.getRequestId());
        mergedParams.put("userId", requestContext.getUserId());

        return new ShoppingCartInstrumentation(logger, mergedParams);
    }
}
