package org.bitbucket.brinkcharl.doo.infrastructure;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
public class RequestContext {
    private String requestId;
    private String userId;
}
