package org.bitbucket.brinkcharl.doo.infrastructure;

import org.bitbucket.brinkcharl.doo.domain.Discount;
import org.slf4j.Logger;

import java.math.BigDecimal;
import java.util.Map;

public class ShoppingCartInstrumentation {
    private static org.slf4j.Logger log;
    private final Map<String, Object> params;

    public ShoppingCartInstrumentation(Logger logger, Map<String, Object> params) {
        ShoppingCartInstrumentation.log = logger;
        this.params = params;
    }

    public void applyingDiscountCode(){
        log.trace("attempting to apply discount code: {}", params.get("discountCode"));
    }

    public void discountCodeLookupFailed(){
        log.warn("discount lookup failed for invalid discount code: {}", params.get("discountCode"));
    }

    public void discountCodeLookupFailed(Exception error){
        log.error("discount lookup failed for discount code: {}: {}", params.get("discountCode"), error);
    }

    public void discountCodeLookupSucceeded(){
        log.trace("discount lookup succeeded for discount code: {}", params.get("discountCode"));
    }

    public void discountApplied(Discount discount, BigDecimal amountDiscounted){
        log.info("discount code applied for code: {}, amount: {}", discount.code(), amountDiscounted);
    }

}
