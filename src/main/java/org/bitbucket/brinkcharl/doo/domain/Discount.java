package org.bitbucket.brinkcharl.doo.domain;

import java.math.BigDecimal;

public interface Discount {
    String code();
    BigDecimal applyToCart(ShoppingCart shoppingCart);
}
