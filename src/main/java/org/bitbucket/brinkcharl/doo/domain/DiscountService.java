package org.bitbucket.brinkcharl.doo.domain;

import java.util.Optional;

public interface DiscountService {
    Optional<Discount> lookupDiscount(String discountCode);
}
