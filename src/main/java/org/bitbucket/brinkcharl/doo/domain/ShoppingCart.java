package org.bitbucket.brinkcharl.doo.domain;

import lombok.AllArgsConstructor;
import org.bitbucket.brinkcharl.doo.infrastructure.ObservationContext;
import org.bitbucket.brinkcharl.doo.infrastructure.ShoppingCartInstrumentation;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@AllArgsConstructor
public class ShoppingCart {
    private final DiscountService discountService;
    private final ObservationContext observationContext;

    public void applyDiscountCode(String discountCode){
        Map<String, Object> extraParams = new HashMap<>();
        extraParams.put("discountCode", discountCode);
        ShoppingCartInstrumentation shoppingCartInstrumentation = observationContext.createShoppingCartInstrumentation(extraParams);
        shoppingCartInstrumentation.applyingDiscountCode();
        Optional<Discount> discount;
        try {
            discount = this.discountService.lookupDiscount(discountCode);
            if (!discount.isPresent()) {
                shoppingCartInstrumentation.discountCodeLookupFailed();
            } else {
                applyDiscount(shoppingCartInstrumentation, discount.get());
            }
        } catch (Exception ex) {
            shoppingCartInstrumentation.discountCodeLookupFailed(ex);
        }
    }

    private void applyDiscount(ShoppingCartInstrumentation shoppingCartInstrumentation, Discount discount) {
        shoppingCartInstrumentation.discountCodeLookupSucceeded();
        BigDecimal amountDiscounted = discount.applyToCart(this);
        shoppingCartInstrumentation.discountApplied(discount, amountDiscounted);
    }
}
