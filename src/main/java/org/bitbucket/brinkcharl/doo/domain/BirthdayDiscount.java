package org.bitbucket.brinkcharl.doo.domain;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
@AllArgsConstructor
public class BirthdayDiscount implements Discount {
    private static final String code = "B0001";
    private final BigDecimal percentage;

    @Override
    public String code() {
        return code;
    }

    @Override
    public BigDecimal applyToCart(ShoppingCart shoppingCart) {
        log.info("Applying discount of {}%", percentage);
        return new BigDecimal("10.00");
    }
}
