# Domain-Oriented Observability

* [Domain-Oriented Observability pattern](https://martinfowler.com/articles/domain-oriented-observability.html)
* [Domain-Oriented Observability: The Decorative Way](http://blog.peterritchie.com/Domain-Oriented-Observability-The-Decorative-Way/)

##Getting Started
```git clone git@bitbucket.org:cbrink/domain-oriented-observability.git```

##Basic implementation
Basic implementation as described by Pete Hodgson on Martin Fowler’s blog.

```git checkout release/basic```

##Decorative implementation
Decorative implementation as described by Peter Ritchie.

```git checkout release/decorator```

